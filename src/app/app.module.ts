import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { QuestionOneComponent } from './question-one/question-one.component';
import { FormsModule } from '@angular/forms';
import { OnlyNumberDirective } from './_directive/OnlyNumber.directive';
import { QuestionTwoComponent } from './question-two/question-two.component';
import { HttpClientModule } from '@angular/common/http';
@NgModule({
  declarations: [
    AppComponent,
      QuestionOneComponent,
      QuestionTwoComponent,
      OnlyNumberDirective,
   ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
